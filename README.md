# Arduino code for Modular Synth #

Some - if not all - modules of a modular synth are easy to build with Arduino.

### Dependencies ###

http://sensorium.github.io/Mozzi/

## Modules ##

### Included ###

* VCO
* ADSR envelope
* Metronome
* LFO

### Future ###

These modules may or may not be added later - due to low quality of Arduino native ADC

* VCF
* VCA

### Hardware ###

The code has been tested and is expected to work in Uno and Nano (Arduino clones).

* Deployment instructions

Schematics will be added later. Some instructions are provided in comments.

### Contribution guidelines ###

Not expected today.

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* More software for creative people and contact information:  [here](http://victorx.eu)