/*

Metro module

 Creates pulses with period defined as a sum of low and high
 state durations. The durations are controlled by pots.
 
 The circuit:
 * Potentiometers attached to analog inputs 0 and 1
 * center pin of the potentiometer to the analog pin
 * one side pin (either one) to ground
 * the other side pin to +5V
 * Gate output is digital output 13
 * Trigger output is digital output 12
 * Random CVs on pwm pins 5 and 6 to control VCO, VCF etc

 Created by Victor X
 http://victorx.eu
 
 This code is in the public domain.
 
*/

int lowPin = A0;
int highPin = A1;
int outPin = 13; // D13 is convenient as it is also connected to LED
int trigPin = 12; // trigger
int pwmPin1 = 5; // random CV voltages to control VCO or VCF
int pwmPin2 = 6; //

// in sec - 1000 msec is very close to 1024
int maxDuration = 5;

void setup() {
  // declare the ledPin as an OUTPUT:
  pinMode(outPin, OUTPUT);
  pinMode(trigPin, OUTPUT);
  randomSeed(analogRead(A2)); // read noise from not connected pin
}

void loop() {
  // set trigger
  digitalWrite(trigPin, HIGH);
  // set random CVs
  analogWrite(pwmPin1, random(0, 255));
  analogWrite(pwmPin2, random(0, 255));
  // read the value from the sensor:
  int value = analogRead(highPin) * maxDuration;
  // reset trigger
  digitalWrite(trigPin, LOW);

  // turn the out on
  digitalWrite(outPin, HIGH);
  // stop the program for <value> milliseconds:
  delay(value);
  // turn the out off:
  digitalWrite(outPin, LOW);
  // stop the program for for <value> milliseconds:
  value = analogRead(lowPin) * maxDuration;
  delay(value);
}

